<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class FormFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 30) as $index)
        {
            DB::table('form_fields')
                ->insert([
                    'name' => $faker->firstName,
                    'family' => $faker->lastName,
                    'age' => $faker->numberBetween($min=0, $max=130),
                    'marriageStatus' => $faker->boolean,
                    'salary' => $faker->numberBetween($min=1500, $max=13000),
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
        }
    }
}
