@extends('layouts.appLayout')
@section('customScript')

@section('content')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <p class="alert alert-danger">
                {{$error}}
            </p>
        @endforeach
    @endif
    @if(!is_null($formData))
        <div class="row py-10 mt-5">
            <div class="col-sm-4">
                <label for="perPage">Per Page Paginator</label>
                <select name="perPage" id="perPage">
                    <option {{$page == "5" ? 'selected' : ''}} value="5">5</option>
                    <option {{$page == "10" ? 'selected' : ''}} value="10">10</option>
                    <option {{$page == "15" ? 'selected' : ''}} value="15">15</option>
                    <option {{$page == "20" ? 'selected' : ''}} value="20">20</option>
                </select>
                @csrf
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm4">
                <div class="dropdown">
                    <label for="search">Search</label>
                    <input class="dropdown-toggle form-control" type="text" id="search" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                    </input>
                    <div class="dropdown-menu" id="userList" aria-labelledby="dropdownMenuButton">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    @if(!is_null($formData['listHeaders']))
                        <th scope="col">#</th>
                        @foreach($formData['listHeaders'] as $headerName)
                            <th scope="col">{{$headerName}}</th>
                        @endforeach
                    @endif
                </tr>
                </thead>
                <tbody>
                @if(!is_null($formData['fields']))
                    @foreach($formData['fields'] as $i => $field)
                        <tr>
                            <th scope="row">{{$i+1}}</th>
                            @foreach($field as $key => $item)
                                @if($formData['fieldsType'][$key] == 'bool')
                                    <td>
                                        @if($item)
                                            <button class='dot-red'></button>
                                        @else
                                            <button class='dot-green'></button>
                                        @endif
                                    </td>
                                @else
                                    <td>{{$item}}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        @if(!is_null($formData['fields']))
            {{ $formData['fields']->links() }}
        @endif
    @endif

@endsection
