jQuery(document).ready(function(){

    jQuery('#search').keyup(function(){
        var query = jQuery(this).val();
        if(query.length > 0)
        {
            if(query != '')
            {
                var _token = jQuery('input[name="_token"]').val();
                jQuery.ajax({
                    url:"http://localhost:8000/liveSearch",
                    method:"POST",
                    data:{query:query, _token:_token},
                    success:function(data){
                        jQuery('#userList').fadeIn();
                        jQuery('#userList').html(data);
                    }
                });
            }
        } else {
            jQuery('#userList').val();
            jQuery('#userList').empty();
            jQuery('#userList').fadeOut();
        }

    });

    // $(document).on('click', 'li', function(){
    //     $('#user_name').val($(this).text());
    //     $('#userList').fadeOut();
    // });

});
