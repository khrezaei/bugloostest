<?php

namespace App\Http\Controllers;

use App\formGenerator\Services\abstractions\IDbRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LiveSearchController extends Controller
{
    /**
     * @var IDbRepository
     */
    private $repository;

    /**
     * LiveSearchController constructor.
     * @param IDbRepository $repository
     */
    public function __construct(IDbRepository $repository)
    {

        $this->repository = $repository;
    }

    function fetch(Request $request)
    {
        try {
            if ($request->get('query')) {
                $query = $request->get('query');
                $data = $this->repository->search($query);
                $output = '<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="display: block;z-index: 1;position:relative; padding-left: 3px;">';
                foreach ($data as $row) {
                    $output .= '
       <li class="dropdown-item"><a href="#">' . $row->name . '</a></li>
       ';
                }
                $output .= '</ul>';
                echo $output;
            }
        }catch (\Exception $e)
        {
            dd($e);
//            abort(500);
        }

    }
}
