<?php

namespace App\Http\Controllers;

use App\formGenerator\Services\abstractions\IConfigReader;
use App\formGenerator\Services\abstractions\IDbRepository;
use App\formGenerator\Services\abstractions\IReceiver;
use http\Message;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var IConfigReader
     */
    private $configReader;
    /**
     * @var IDbRepository
     */
    private $repository;
    /**
     * @var IReceiver
     */
    private $receiver;

    /**
     * HomeController constructor.
     * @param IReceiver $receiver
     */
    public function __construct(IReceiver $receiver)
    {

        $this->receiver = $receiver;
    }

    public function index(Request $request)
    {
        try {
            $formData = $this->receiver->getFields($request->get('perPage'), 'name');
            return view('home.index',
                [
                    'formData' => $formData ,
                    'page' => $request->get('perPage')
                ]);
        }catch (\Exception $e)
        {
            if(env('APP_DEBUG', true))
            {
                return view('home.index')
                    ->with('formData', null)
                    ->withErrors('The fields Are not set probably' . $e->getMessage());
            }
            return abort(500);

        }




    }
}
