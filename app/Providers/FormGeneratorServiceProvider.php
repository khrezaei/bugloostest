<?php

namespace App\Providers;

use App\formGenerator\Services\abstractions\IConfigReader;
use App\formGenerator\Services\abstractions\IDbRepository;
use App\formGenerator\Services\abstractions\IReceiver;
use App\formGenerator\Services\DbMysqlRepository;
use App\formGenerator\Services\GetConfigFromFile;
use App\formGenerator\Services\ReceiveFromDb;
use Illuminate\Support\ServiceProvider;

class FormGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IConfigReader::class,
            GetConfigFromFile::class
        );
        $this->app->bind(
            IReceiver::class,
            ReceiveFromDb::class
        );
        $this->app->bind(
            IDbRepository::class,
            DbMysqlRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
