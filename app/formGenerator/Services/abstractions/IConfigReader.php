<?php


namespace App\formGenerator\Services\abstractions;


interface IConfigReader
{
    public function getFields();
    public function getListHeaders();
    public function getSortables();
    public function lengthOnView();
    public function searchAbles();
    public function getFieldsType();
}
