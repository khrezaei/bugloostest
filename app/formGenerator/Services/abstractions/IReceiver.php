<?php


namespace App\formGenerator\Services\abstractions;


interface IReceiver
{
    public function getFields($perPage, $sortBy);
}
