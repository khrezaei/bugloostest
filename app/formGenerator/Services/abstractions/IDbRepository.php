<?php


namespace App\formGenerator\Services\abstractions;


interface IDbRepository
{
    public function getItems($fields, int $perPage, $sortBy);
    public function search($searchValue);
}
