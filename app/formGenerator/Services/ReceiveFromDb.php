<?php


namespace App\formGenerator\Services;


use App\formGenerator\Services\abstractions\IConfigReader;
use App\formGenerator\Services\abstractions\IDbRepository;
use App\formGenerator\Services\abstractions\IReceiver;

class ReceiveFromDb implements IReceiver
{
    /**
     * @var IConfigReader
     */
    private $configReader;
    /**
     * @var IDbRepository
     */
    private $repository;

    /**
     * ReceiveFromDb constructor.
     * @param IConfigReader $configReader
     * @param IDbRepository $repository
     */
    public function __construct(IConfigReader $configReader, IDbRepository $repository)
    {
        $this->configReader = $configReader;
        $this->repository = $repository;
    }

    public function getFields($perPage = 5, $sortBy)
    {
        try {
            $values = [
                "fields" => $this->repository->getItems($this->configReader->getFields(), $perPage, $sortBy),
                "listHeaders" => $this->configReader->getListHeaders(),
                "lengthOnView" => $this->configReader->lengthOnView(),
                "searchAbles" =>  $this->configReader->searchAbles(),
                "sortAbles" =>  $this->configReader->getSortables(),
                "fieldsType" => $this->configReader->getFieldsType()
            ];
            return $values;
        } catch (\Exception $e){
            throw $e;
        }

    }

}
