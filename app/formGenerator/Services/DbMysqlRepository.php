<?php


namespace App\formGenerator\Services;


use App\formGenerator\Services\abstractions\IConfigReader;
use Illuminate\Support\Facades\DB;

class DbMysqlRepository implements abstractions\IDbRepository
{
    /**
     * @var IConfigReader
     */
    private $configReader;

    /**
     * DbMysqlRepository constructor.
     * @param IConfigReader $configReader
     */
    public function __construct(IConfigReader $configReader)
    {
        $this->configReader = $configReader;
    }

    public function getItems($fields, $perPage, $sortBy)
    {
        try {
            if (is_null($perPage)) {
                $perPage = 5;
            }
            if(!is_null($sortBy))
                $sortAbles = $this->configReader->getSortables();
            {
                foreach ($sortAbles as $sortAble)
                {
                    if($sortAble == $sortBy)
                    {
                        return DB::table('form_fields')
                            ->select($fields)
                            ->orderBy($sortBy)
                            ->paginate($perPage);
                    }
                }
            }
            return DB::table('form_fields')
                ->select($fields)
                ->paginate($perPage);
        } catch (\Exception $e){
            throw $e;
        }
    }

    public function search($searchValue)
    {
        try {
            $fields = $this->configReader->searchAbles();
            if (count($fields) > 0) {
                $query = "select * from bugloos.form_fields where ";

                foreach ($fields as $i => $field) {
                    if (count($fields) == $i+1) {
                        $query .= $field . " LIKE " . "'%" . $searchValue . "%' ;";
                    } else
                    {
                        $query .= $field . " LIKE " . "'%" . $searchValue . "%' " . "or ";
                    }
                }
                return DB::select(DB::raw($query));

            }
        }catch (\Exception $e){
            throw new Exception($e);
        }
    }
}
