<?php


namespace App\formGenerator\Services;


use Illuminate\Support\Facades\Config;

class GetConfigFromFile implements abstractions\IConfigReader
{


    public function getFields()
    {
        try {
            return Config::get('formConfig.fields');
        }  catch (\Exception $e){
            throw new Exception($e);
        }

    }

    public function getSortables()
    {
        try {
            return Config::get('formConfig.sortAbles');
        } catch (\Exception $e){
            throw new Exception($e);
        }

    }

    public function getListHeaders()
    {
        try {
            return Config::get('formConfig.headers');
        }catch (\Exception $e){
            throw new Exception($e);
        }

    }

    public function lengthOnView()
    {
        try {
            return Config::get('formConfig.lengthOnView');
        } catch (\Exception $e){
            throw new Exception($e);
        }

    }

    public function searchAbles()
    {
        try {
            return Config::get('formConfig.searchAbles');
        }catch (\Exception $e){
            throw new Exception($e);
        }
    }

    public function getFieldsType()
    {
        try {
            return Config::get('formConfig.fieldTypes');
        } catch (\Exception $e){
            throw new Exception($e);
        }

    }
}
