<?php

return [
    "fields" => [
        "name",
        "family",
        "age",
        "marriageStatus"
    ],
    "headers" => [
        "Name",
        "Family",
        "Age",
        "Marriage Status"
    ],
    "sortAbles" => [
        "family",
    ],
    "lengthOnView" => [
        "name" => 25,
        "family" => 25,
        "age" => 15
    ],
    "searchAbles" => [
        "family",
        "name",
    ],
    "fieldTypes" => [
        'name' => 'string',
        'family' => 'string',
        'age' => 'number',
        'marriageStatus' =>'bool'
    ]
];
